INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

Set (THREADS_PREFER_PTHREAD_FLAG ON)
find_package (Threads REQUIRED)

SET(CLIENTE_SRCS 
	MundoCliente.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
			
SET(SERVIDOR_SRCS
	MundoServidor.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)
	
ADD_EXECUTABLE(cliente Cliente.cpp  ${CLIENTE_SRCS})
ADD_EXECUTABLE(servidor Servidor.cpp ${SERVIDOR_SRCS})
ADD_EXECUTABLE(bot bot.cpp)
ADD_EXECUTABLE(logger logger.cpp)

TARGET_LINK_LIBRARIES(servidor Threads::Threads glut GL GLU)
TARGET_LINK_LIBRARIES(cliente Threads::Threads glut GL GLU)
